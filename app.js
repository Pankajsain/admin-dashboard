const express = require("express");
const path = require("path");
const hbs = require("hbs");

const app = express();

app.use(express.json());


const template_partials = path.join(__dirname, "templates/partials/");
const template_view = path.join(__dirname, "templates/views");
app.use(express.static(path.join(__dirname, "public")));

app.set("view engine", "hbs");
app.set("views", template_view);
hbs.registerPartials(template_partials);

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/allMovies", (req, res) => {
  res.render("allMovies");
});

app.get("/addMovie", (req, res) => {
  res.render("addMovie");
});

app.get("/allUsers", (req, res) => {
  res.render("allUsers");
});

app.get("/profile", (req, res) => {
  res.render("profile");
});

app.get("/addCategory", (req, res) => {
  res.render("addCategory");
});

app.get("/allCategories", (req, res) => {
  res.render("allCategories");
});

app.get("/addReward", (req, res) => {
  res.render("addReward");
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log("server is running....");
  });
  